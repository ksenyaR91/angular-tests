import { Post } from './post';
import { Component, OnInit,EventEmitter,Output} from '@angular/core';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
   post: Post;


 @Output()deleteEvent = new EventEmitter<Post>();
  isEdit:Boolean = false;
  editButtonText = "Edit";
  
   constructor() { }
   previousBody;
   previousTitle;
  sendDelete(){
    this.deleteEvent.emit(this.post);
  }
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }

saveOldPost()
{
  this.previousBody=this.post.body;
  this.previousTitle=this.post.title;
}

saveNewPost(){
 // this.editEvent.emit(this.post);
}

cancelChanges(){
  this.post.title = this.previousTitle;
   this.post.body = this.previousBody;
   this.isEdit=!this.isEdit;
  this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
 }

  ngOnInit() {
  }

}
