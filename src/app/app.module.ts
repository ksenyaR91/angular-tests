import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.Service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserComponent } from './user/user.component';
import { PostFormComponent } from './post-form/post-form.component';
import { UserPostComponent } from './user-post/user-post.component';
import { UsersService } from './users/users.service';
import{AngularFireModule} from 'angularfire2';


export const firebaseConfig = {
   apiKey: "AIzaSyAGjZ5MkGCi1JDE8vs1TGUf5xKvM6jVOwA",
    authDomain: "project-cb388.firebaseapp.com",
    databaseURL: "https://project-cb388.firebaseio.com",
    storageBucket: "project-cb388.appspot.com",
    messagingSenderId: "22182806630"
    };
  

 




@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserComponent,
    PostFormComponent,
    UserPostComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
  //  RouterModule.forRoot(appRoutes)
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
