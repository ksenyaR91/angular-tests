import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})


export class PostsComponent implements OnInit {
  posts;
  currentPosts;
  isLoading:Boolean = true;
  select(post){this.currentPosts = post}

  constructor(private _postsService:PostsService) { }
  addPost(post){
    this._postsService.addPost(post);
  }
  deletePost(post){
    this._postsService.deletePost(post);
    
  }
editPost(post){
this.posts.splice(
    this.posts.indexOf(post),0
   )
}
updatePost(post){
    this._postsService.updatePost(post);
  }


  ngOnInit() {
      this._postsService.getPosts().subscribe(postsData =>

   {this.posts = postsData 
          this.isLoading = false})
          console.log(this.posts)};
  }


