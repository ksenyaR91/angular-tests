import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';


@Injectable()
export class PostsService {
  //private _url = 'http://jsonplaceholder.typicode.com/posts';
  postsObservabale;
  getPosts(){
 
  this.postsObservabale = this.af.database.list('/posts');
  return this.postsObservabale;

  }
    addPost(post){
  this.postsObservabale.push(post);
}
updatePost(post){

  let postKey= post.$key;
  let postData = {body:post.body,title:post.title};
  this.af.database.object('/posts/' + postKey).update(postData);
}

deletePost(post){
let postKey= post.$key;
this.af.database.object('/posts/' + postKey).remove();

}
constructor(private af:AngularFire) { }
  


 // constructor(private _http:Http) { }

}
